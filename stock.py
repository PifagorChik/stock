# Импортируем MongoClient из pymongo
from pymongo import MongoClient

# Создаем клиента и подключаемся к базе
client = MongoClient()
# Создаем базу данных "stocks"
db = client['stocks']
